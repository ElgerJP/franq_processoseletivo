from django.db import models
from django.db.models.fields import AutoField, CharField
from django.db.models.fields.related import ForeignKey
from django.dispatch import receiver
from django.db.models.signals import post_save

from Pessoa.models import Pessoa

# Create your models here.
class GaragemManager(models.Manager):
    def by_pessoa(self, dono: Pessoa):
        return super().get_queryset().filter(dono=dono)


# Model for Garagem
class Garagem(models.Model):
    garagem_id = AutoField(primary_key=True)
    dono = ForeignKey(Pessoa, on_delete=models.CASCADE, null=True)
    nome = CharField(max_length=50)
    objects = models.Manager()
    custom_manager = GaragemManager()

    def __str__(self):
        return f"{self.nome}"


# Create one empty Garagem for each new user
@receiver(post_save, sender=Pessoa)
def create_garagem_on_user(sender, instance=None, created=False, **kwargs):
    if created:
        Garagem.objects.create(dono=instance, nome="DEFAULT GARAGEM")


class VeiculoManager(models.Manager):
    def by_garagem(self, garagem: Garagem):
        return super().get_queryset().filter(garagem=garagem)

    def by_pessoa(self, pessoa: Pessoa):
        garagens = Garagem.custom_manager.by_pessoa(dono=pessoa)
        veiculos = [
            [veiculo for veiculo in Veiculo.custom_manager.by_garagem(garagem=garagem)]
            for garagem in garagens
        ]

        return [item for sublist in veiculos for item in sublist]


class Veiculo(models.Model):
    garagem = ForeignKey(Garagem, on_delete=models.CASCADE)
    modelo = CharField(max_length=50)
    ano = models.IntegerField()
    VEHICLE_TYPE = (("MOTO", "Moto"), ("CARRO", "Carro"))
    tipo = CharField(max_length=5, choices=VEHICLE_TYPE)
    cor = CharField(max_length=50)
    custom_manager = VeiculoManager()

    def __str__(self):
        return f"{self.tipo} {self.modelo}, {self.ano}"

    def info(self):
        return (
            f"{self.modelo}, {self.ano}"
            if self.tipo == "MOTO"
            else f"{self.cor}, {self.ano}"
        )
