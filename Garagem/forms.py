from django import forms
from django.forms import ModelChoiceField

from Garagem.models import Veiculo, Garagem
from Pessoa.models import Pessoa


class VeiculoForm(forms.ModelForm):
    class Meta:
        model = Veiculo
        fields = ("garagem", "modelo", "ano", "tipo", "cor")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(VeiculoForm, self).__init__(*args, **kwargs)
        filtered_options = Garagem.objects.filter(
            dono=Pessoa.objects.get(user=self.request.user)
        )

        self.fields["garagem"] = ModelChoiceField(queryset=filtered_options)
