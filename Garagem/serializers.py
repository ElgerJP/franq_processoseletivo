from rest_framework import serializers

from Garagem.models import Garagem


class GaragemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garagem
        fields = ("garagem_id", "dono", "nome")
