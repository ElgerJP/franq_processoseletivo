from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from Garagem.forms import VeiculoForm
from Garagem.models import Garagem, Veiculo
from Pessoa.models import Pessoa

import pandas as pd


# Create your views here.
class ShowUsers(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        usuarios = [
            {
                "id": pessoa.user.id,
                "info": {
                    "nome": pessoa.nome,
                    "garagem": [
                        {
                            "nome": garagem.nome,
                            "veiculos": [
                                veiculo.info()
                                for veiculo in Veiculo.custom_manager.by_garagem(
                                    garagem
                                )
                            ],
                        }
                        for garagem in Garagem.custom_manager.by_pessoa(pessoa)
                    ],
                },
            }
            for pessoa in Pessoa.objects.all()
        ]

        return Response({"result": usuarios})


@csrf_exempt
@login_required
def vehicle_checker(request):
    if request.method == "GET":

        veiculos = [
            veiculo.info()
            for veiculo in Veiculo.custom_manager.by_pessoa(
                Pessoa.objects.get(user=request.user)
            )
        ]

        result = {
            "vehicles": pd.DataFrame(data=veiculos, columns=["VEICULOS"]).to_html()
        }
        template = "vehicles.html"

        return render(request, template, result)


@csrf_exempt
@login_required
def garage_checker(request):
    if request.method == "GET":

        garagens = Garagem.custom_manager.by_pessoa(
            dono=Pessoa.objects.get(user=request.user)
        )

        result = {
            garagem.nome: [
                veiculo.info()
                for veiculo in Veiculo.custom_manager.by_garagem(garagem=garagem)
            ]
            for garagem in garagens
        }

        result = {"garagens": pd.DataFrame.from_dict(result, orient="index").to_html()}
        template = "garagem.html"

        return render(request, template, result)


@login_required
@csrf_exempt
def add_vehicle(request):
    if request.method == "POST":
        form = VeiculoForm(data=request.POST, request=request)

        if form.is_valid():
            vehicle = form.save(commit=False)
            vehicle.save()

            return redirect(f"add_vehicle")

    form = VeiculoForm(request=request)

    context = {"form": form}
    template = "add_vehicle.html"

    return render(request, template, context)
