from django.urls import path

from rest_framework.authtoken.views import obtain_auth_token

from Garagem import views

urlpatterns = [
    path("add_vehicle/", views.add_vehicle, name="add_vehicle"),
    path("garagem/", views.garage_checker, name="garage_checker"),
    path("vehicles/", views.vehicle_checker, name="vehicle_checker"),
    path("api/", views.ShowUsers.as_view(), name="api"),
    path("api-token-auth/", obtain_auth_token, name="api_token_auth"),
]
