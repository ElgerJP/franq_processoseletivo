from django.contrib import admin

from Garagem.models import Garagem, Veiculo

# Register your models here.
admin.site.register(Garagem)
admin.site.register(Veiculo)
