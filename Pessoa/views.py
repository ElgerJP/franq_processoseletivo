from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login

from Pessoa.forms import UserForm, PessoaForm

# # Create your views here.
@csrf_exempt
def register(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        profile_form = PessoaForm(request.POST)

        if form.is_valid() and profile_form.is_valid():
            user = form.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()

            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password1")

            user = authenticate(username=username, password=password)
            login(request, user)

            return redirect("home")

    user_form = UserForm()
    profile_form = PessoaForm()

    context = {"form": user_form, "profile_form": profile_form}
    template = "registration/register.html"

    return render(request, template, context)
