from django.db import models
from django.contrib.auth.models import User

from Pessoa.validators import validador_telefone


# Create your models here.
class Pessoa(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    nome = models.CharField(max_length=50)
    telefone = models.CharField(validators=[validador_telefone], max_length=15)

    def __str__(self):
        return f"{self.user.username} | {self.user.email}"
