from django.core.validators import RegexValidator

# Only allow the following pattern for brazilian cellphone numbers: "(DDD) 9XYZW-ABCD"
validador_telefone = RegexValidator(
    r"^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$"
)
